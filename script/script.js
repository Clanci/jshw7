function filterBy(arr, dataType) {
    return arr.filter(item => typeof item !== dataType);
}

const inputArray = ['hello', 15, 23, '23', null];
const filteredArray = filterBy(inputArray, 'string');
console.log(filteredArray);